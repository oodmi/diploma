#!/usr/bin/env bash

java -Xms128m -Xmx"$MEMORY_MAX" \
    -Dspring.profiles.active="$PROFILES" -Dserver.context-path="$CONTEXT" \
    -jar diploma.war
