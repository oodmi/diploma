CREATE TABLE session
(
  id    BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name  VARCHAR(256),
  start DATETIME           NOT NULL,
  to   DATETIME           NOT NULL
);
CREATE TABLE graph
(
  id         INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name       VARCHAR(256),
  all_time   BIGINT             NOT NULL,
  session_id BIGINT            NOT NULL,
  CONSTRAINT graph_session_id_fk FOREIGN KEY (session_id) REFERENCES session (id)
);
# CREATE INDEX graph_session_id_fk ON graph (session_id);
CREATE TABLE node
(
  id         BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  graph_id   BIGINT             NOT NULL,
  x          DOUBLE             NOT NULL,
  y          DOUBLE             NOT NULL,
  info       LONGTEXT           NOT NULL,
  time       BIGINT             NOT NULL,
  dispersion DOUBLE             NOT NULL,
  median     DOUBLE             NOT NULL,
  average    DOUBLE             NOT NULL,
  CONSTRAINT node_graph_id_fk FOREIGN KEY (graph_id) REFERENCES graph (id)
);
# CREATE INDEX node_graph_id_fk ON node (graph_id);
CREATE TABLE edge
(
  id       BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  graph_id BIGINT             NOT NULL,
  from    BIGINT             NOT NULL,
  to      BIGINT             NOT NULL,
  step     BIGINT             NOT NULL,
  CONSTRAINT edge_graph_id_fk FOREIGN KEY (graph_id) REFERENCES graph (id),
  CONSTRAINT edge_node_id_fk_begin FOREIGN KEY (from) REFERENCES node (id),
  CONSTRAINT edge_node_id_fk_end FOREIGN KEY (to) REFERENCES node (id)
);
# CREATE INDEX edge_graph_id_fk ON edge (graph_id);
# CREATE INDEX edge_node_id_fk_begin ON edge (from);
# CREATE INDEX edge_node_id_fk_end ON edge (to);