package amm.vsu.diploma.syslog;

import amm.vsu.diploma.cluster.ClusteringProcessor;
import amm.vsu.diploma.domain.model.info.ExecutionInfo;
import amm.vsu.diploma.domain.model.info.TransactionStatementsInfo;
import amm.vsu.diploma.enums.Status;
import amm.vsu.diploma.utils.JsonUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

class TransactionAccumulator {
    private static Logger logger = LoggerFactory.getLogger(TransactionAccumulator.class);
    private static TransactionAccumulator instance = new TransactionAccumulator();
    private ConcurrentHashMap<Long, TransactionStatementsInfo> transactions = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Long, Set<Date>> udpTimes = new ConcurrentHashMap<>();

    static TransactionAccumulator getInstance() {
        return instance;
    }

    void handle(String syslogMessage) throws ParseException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        Map<String, Object> jsonParametersMap;
        try {
            jsonParametersMap = JsonUtils.getJsonParametersMap(syslogMessage);
        } catch (Exception e) {
            throw new ParseException(e.getMessage(), 0);
        }
        ExecutionInfo executionInfo = mapper.convertValue(jsonParametersMap, ExecutionInfo.class);
        Set<Date> existsMessages = udpTimes.computeIfAbsent(executionInfo.getTid(), t -> new HashSet<>());
        if (!existsMessages.add(executionInfo.getDate()))
            return;
        String statement = executionInfo.getQuery().get(0);
        TransactionStatementsInfo transactionStatementsInfo =
                transactions.computeIfAbsent(executionInfo.getTid(), t -> new TransactionStatementsInfo(executionInfo.getTid()));
        if (statement.startsWith("commit")) {
            transactionStatementsInfo.setStatus(Status.COMMITED);
            String countOp = statement.substring(statement.indexOf(":") + 1, statement.length());
            transactionStatementsInfo.setCountStatusStatements(Integer.parseInt(countOp));
            transactionStatementsInfo.setDateEnd(executionInfo.getDate());
        } else if (statement.startsWith("rollback")) {
            transactionStatementsInfo.setStatus(Status.ROLLBACKED);
            String countOp = statement.substring(statement.indexOf(":") + 1, statement.length());
            transactionStatementsInfo.setDateEnd(executionInfo.getDate());
            transactionStatementsInfo.setCountStatusStatements(Integer.parseInt(countOp));
        } else {
            transactionStatementsInfo.getStatements().add(executionInfo);
        }
        if (transactionStatementsInfo.isFull()) {
            TransactionClusteringTask task =
                    new TransactionClusteringTask(transactionStatementsInfo,
                            ClusteringProcessor.getInstance());
            task.start();
            transactions.remove(transactionStatementsInfo.getTid());
        }
    }
}
