package amm.vsu.diploma.syslog;

import amm.vsu.diploma.cluster.ClusteringProcessor;
import amm.vsu.diploma.domain.model.info.TransactionStatementsInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class TransactionClusteringTask extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(TransactionClusteringTask.class);
    private TransactionStatementsInfo transactionStatementsInfo;
    private ClusteringProcessor clusteringProcessor;

    TransactionClusteringTask(TransactionStatementsInfo transactionStatementsInfo,
                              ClusteringProcessor clusteringProcessor) {
        this.transactionStatementsInfo = transactionStatementsInfo;
        this.clusteringProcessor = clusteringProcessor;
    }

    @Override
    public void run() {
        logger.debug("Start clustering {}", transactionStatementsInfo);
        clusteringProcessor.addTransaction(transactionStatementsInfo);
    }
}
