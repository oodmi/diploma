package amm.vsu.diploma.syslog;

import org.productivity.java.syslog4j.server.SyslogServer;
import org.productivity.java.syslog4j.server.SyslogServerIF;

public class SyslogReceiver {
    private static String host = "127.0.0.1";
    private static int port = 514;
    private SyslogServerIF serverUdp = SyslogServer.getInstance("udp");

    public void start() {
        serverUdp.getConfig().addEventHandler(new SyslogTransactionMessageHandler());
        serverUdp.getConfig().setHost(host);
        serverUdp.getConfig().setPort(port);
        serverUdp.run();
    }

    public void stop() {
        serverUdp.shutdown();
    }
}
