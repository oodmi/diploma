package amm.vsu.diploma.syslog;

import org.productivity.java.syslog4j.server.SyslogServerEventHandlerIF;
import org.productivity.java.syslog4j.server.SyslogServerEventIF;
import org.productivity.java.syslog4j.server.SyslogServerIF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;

class SyslogTransactionMessageHandler implements SyslogServerEventHandlerIF {
    private static Logger logger = LoggerFactory.getLogger(SyslogTransactionMessageHandler.class);
    private TransactionAccumulator accumulator = TransactionAccumulator.getInstance();

    public void event(SyslogServerIF syslogServer, SyslogServerEventIF event) {
        String message = event.getMessage();
        logger.debug("message = {}", message);
        try {
            accumulator.handle(message);
        } catch (ParseException e) {
            logger.debug("exception {}", e);
        }
    }
}
