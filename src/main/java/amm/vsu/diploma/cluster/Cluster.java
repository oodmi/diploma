package amm.vsu.diploma.cluster;


import amm.vsu.diploma.domain.model.info.TransactionStatementsInfo;

import java.util.HashMap;
import java.util.Map;

public class Cluster {
    private ClusterGraph clusterGraph;
    private Long number;
    private Map<Long, TransactionStatementsInfo> transactions;

    public Cluster(TransactionStatementsInfo transactionStatementsInfo, Long number) {
        this.clusterGraph = new ClusterGraph(transactionStatementsInfo);
        this.number = number;
        this.transactions = new HashMap<>();
        transactions.put(transactionStatementsInfo.getTid(), transactionStatementsInfo);
    }
    /**
     * Distance between transaction graph and cluster.
     *
     * @param transactionGraph
     * @return
     */
    public double getDistance(ClusterGraph transactionGraph) {
        return clusterGraph.getDistance(transactionGraph);
    }

    public double getDistance(Cluster cluster) {
        return clusterGraph.getDistance(cluster.getClusterGraph());
    }

    /**
     * Add cluster graph.
     *
     * @param clusterGraph
     */
    public void add(ClusterGraph clusterGraph) {
        this.clusterGraph.merge(clusterGraph);
    }

    public void merge(Cluster cluster) {
        this.transactions.putAll(cluster.transactions);
        this.clusterGraph.merge(cluster.clusterGraph);
    }

    public void add(TransactionStatementsInfo info) {
        ClusterGraph graph = new ClusterGraph(info);
        clusterGraph.merge(graph);
        transactions.put(info.getTid(), info);
    }

    /**
     * check cluster has statement.
     *
     * @param value    value
     * @param isRegexp is regexp
     * @return true if present.
     */
    public boolean hasStatement(String value, boolean isRegexp) {
        return clusterGraph.hasStatement(value, isRegexp);
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public ClusterGraph getClusterGraph() {
        return clusterGraph;
    }

    public void setClusterGraph(ClusterGraph clusterGraph) {
        this.clusterGraph = clusterGraph;
    }

    @Override
    public String toString() {
        return "Cluster {" +
                "clusterGraph=" + clusterGraph +
                '}';
    }
}
