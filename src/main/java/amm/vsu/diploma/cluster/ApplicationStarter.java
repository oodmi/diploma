package amm.vsu.diploma.cluster;

import amm.vsu.diploma.domain.model.StartParams;
import amm.vsu.diploma.syslog.SyslogReceiver;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ApplicationStarter {
    private SyslogReceiver syslogReceiver = new SyslogReceiver();
    private ClusteringProcessor clusteringProcessor = ClusteringProcessor.getInstance();
    private Thread syslogReceiverThread;
    private AppStatus status = AppStatus.STOPPED;
    private Date start;
    private Date end;

    public AppStatus getStatus() {
        return status;
    }

    public ClusteringProcessor getClusteringProcessor() {
        return clusteringProcessor;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    /**
     * Start clustering process and message receiver.
     *
     * @param startParams start parameters.
     */
    public void start(StartParams startParams) {
        if (status == AppStatus.STOPPED) {
            clusteringProcessor.setMaxClusterSize(startParams.getMaxClusters());
            clusteringProcessor.setMinThreshold(startParams.getMinThreshold());
            clusteringProcessor.clear();
            syslogReceiverThread = new Thread(syslogReceiver::start);
            syslogReceiverThread.start();
            status = AppStatus.EXECUTE;
            start = new Date();
            end = null;
        }
    }

    /**
     * Stop parameters.
     */
    public void stop() {
        if (status == AppStatus.EXECUTE) {
            syslogReceiver.stop();
            syslogReceiverThread.interrupt();
            status = AppStatus.STOPPED;
            end = new Date();
        }
    }

    public enum AppStatus {
        EXECUTE("Выполняется"),
        STOPPED("Остановлено"),
        SAVED("Сохранено");
        String value;

        AppStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
