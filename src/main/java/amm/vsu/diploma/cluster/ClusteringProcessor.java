package amm.vsu.diploma.cluster;


import amm.vsu.diploma.domain.model.info.TransactionStatementsInfo;
import amm.vsu.diploma.enums.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clustering processor.
 */
public class ClusteringProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClusteringProcessor.class);
    private static ClusteringProcessor instance = new ClusteringProcessor();
    private List<Cluster> clusters = new ArrayList<>();
    private double minThreshold = 0.2f;
    private int maxClusterSize = 5;
    private int transactionsCommit = 0;
    private int transactionsRollback = 0;

    public static ClusteringProcessor getInstance() {
        return instance;
    }

    public double getMinThreshold() {
        return minThreshold;
    }

    public void setMinThreshold(double minThreshold) {
        this.minThreshold = minThreshold;
    }

    public int getMaxClusterSize() {
        return maxClusterSize;
    }

    public void setMaxClusterSize(int maxClusterSize) {
        this.maxClusterSize = maxClusterSize;
    }

    public int getTransactionsCommit() {
        return transactionsCommit;
    }

    public int getTransactionsRollback() {
        return transactionsRollback;
    }

    public synchronized void addTransaction(TransactionStatementsInfo transactionStatementsInfo) {
        if (transactionStatementsInfo.getStatus().equals(Status.COMMITED))
            transactionsCommit++;
        else if (transactionStatementsInfo.getStatus().equals(Status.ROLLBACKED))
            transactionsRollback++;

        ClusterGraph transactionGraph = new ClusterGraph(transactionStatementsInfo);
        if (clusters.size() < maxClusterSize) {
            clusters.add(new Cluster(transactionStatementsInfo, (long) (clusters.size() + 1)));
            LOGGER.debug("Transaction {} was added in cluster {}",
                    transactionGraph.getStatementsSet(), clusters.size());
        } else {
            double currentMetric = 1.0f;
            int index = 0;
            for (int i = 0; i < clusters.size(); i++) {
                double v = clusters.get(i).getDistance(transactionGraph);
                if (v < currentMetric) {
                    currentMetric = v;
                    index = i;
                }
            }
            if (currentMetric <= minThreshold) {
                LOGGER.debug("Transaction {} was added in cluster {} distance {}",
                        transactionGraph.getStatementsSet(), clusters.get(index), currentMetric);
                clusters.get(index).add(transactionStatementsInfo);
            } else {
                clusters.add(new Cluster(transactionStatementsInfo, (long) (clusters.size() + 1)));
                restructure();
            }
        }
    }

    public List<Cluster> getClusters() {
        return clusters;
    }

    /**
     * Trying to merge clusters with distance < minThreshold.
     */
    private void restructure() {
        LOGGER.debug("starting restructure of clusters.");
        boolean[] visited = new boolean[clusters.size()];
        for (int i = 0; i < clusters.size(); i++) {
            if (visited[i])
                continue;
            Cluster clusterA = clusters.get(i);
            double minDistance = 1.0f;
            int index = clusters.size() + 1;
            for (int j = i + 1; j < clusters.size(); j++) {
                if (visited[j])
                    continue;
                Cluster clusterB = clusters.get(j);
                double distance = clusterA.getDistance(clusterB.getClusterGraph());
                if (distance < minDistance) {
                    minDistance = distance;
                    index = j;
                }
            }
            if (minDistance < minThreshold) {
                clusterA.merge(clusters.get(index));
                clusters.set(index, null);
                visited[i] = visited[index] = true;
                LOGGER.debug("merged cluster with numbers  {} {}", i, index);
            }
        }
        clusters = this.clusters.stream().filter(k -> k != null).collect(Collectors.toList());
        int i = 0;
        for (Cluster cluster : clusters) {
            cluster.setNumber((long) ++i);
        }
        LOGGER.debug("restructure of clusters finished");
    }

    /**
     * Clear clusters.
     */
    public void clear() {
        clusters = new ArrayList<>();
        transactionsCommit = 0;
        transactionsRollback = 0;
    }

    /**
     * Get clusters contains value or regexp.
     *
     * @param value
     * @param isRegexp
     * @return
     */
    public List<Cluster> getClustersContainsStatement(String value, boolean isRegexp) {
        return clusters.stream().filter(c -> c.hasStatement(value, isRegexp)).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        StringBuilder cluster = new StringBuilder();
        for (int i = 0; i < clusters.size(); i++) {
            cluster.append("#").append(i).append(" ").append(clusters.get(i)).append("\n");
        }
        return cluster.toString();
    }
}
