package amm.vsu.diploma.cluster;

import amm.vsu.diploma.domain.model.info.ExecutionInfo;
import amm.vsu.diploma.domain.model.info.TransactionStatementsInfo;
import com.google.common.collect.Sets;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Cluster graph.
 */
public class ClusterGraph {
    private Map<String, Map<String, Integer>> entry;
    private Set<String> statementsSet;

    public ClusterGraph(Map<String, Map<String, Integer>> entry) {
        this.entry = entry;
    }

    public ClusterGraph() {
        entry = new HashMap<>();
        statementsSet = new HashSet<>();
    }

    public ClusterGraph(TransactionStatementsInfo transactionStatementsInfo) {
        this();
        SortedSet<ExecutionInfo> statementInfos = transactionStatementsInfo.getStatements();
        List<String> statements = new ArrayList<>();
        statementInfos.forEach(executionInfo -> executionInfo.getQuery().forEach(statements::add));
        createGraphByStatementList(statements);
    }

    public void createGraphByStatementList(List<String> statements) {
        int index = 0;
        Map<String, Integer> statementIndex = new HashMap<>();
        Map<Integer, String> indexStatement = new HashMap<>();
        for (String statement : statements) {
            if (!statementIndex.containsKey(statement)) {
                indexStatement.put(index, statement);
                statementIndex.put(statement, index);
                index++;
            }
            statementsSet.add(statement);
        }
        int[][] graph = new int[index][index];
        for (int i = 1; i < statements.size(); i++) {
            int indexFrom = statementIndex.get(statements.get(i - 1));
            int indexTo = statementIndex.get(statements.get(i));
            graph[indexFrom][indexTo]++;
        }
        for (int i = 0; i < graph.length; i++) {
            String statement = indexStatement.get(i);
            entry.putIfAbsent(statement, new HashMap<String, Integer>());
            for (int j = 0; j < graph[i].length; j++) {
                if (graph[i][j] != 0) {
                    entry.get(statement).put(indexStatement.get(j), graph[i][j]);
                }
            }
        }
    }

    public Map<String, Map<String, Integer>> getEntry() {
        return entry;
    }

    public Set<String> getStatementsSet() {
        return statementsSet;
    }

    public void setStatementsSet(Set<String> statementsSet) {
        this.statementsSet = statementsSet;
    }

    /**
     * Merge with graph.
     *
     * @param graph
     */
    public void merge(ClusterGraph graph) {
        Map<String, Map<String, Integer>> aEntry = graph.getEntry();
        for (String key : aEntry.keySet()) {
            entry.putIfAbsent(key, new HashMap<String, Integer>());
            Map<String, Integer> statements = aEntry.get(key);
            for (Map.Entry<String, Integer> statementEntry : statements.entrySet()) {
                Map<String, Integer> keyStatement = entry.get(key);
                Integer statementCount = keyStatement.get(statementEntry.getKey());
                if (statementCount == null) {
                    statementCount = 0;
                    statementsSet.add(statementEntry.getKey());
                }
                keyStatement.put(statementEntry.getKey(), statementCount + statementEntry.getValue());
            }
        }
    }

    /**
     * Check graph has statement or regexp.
     *
     * @param sqlStatement - statement or regexp
     * @param isRegexp     true if sqlStatement is regexp
     * @return true if contains with statement.
     */
    public boolean hasStatement(String sqlStatement, boolean isRegexp) {
        if (isRegexp) {
            Pattern pattern = Pattern.compile(sqlStatement);
            Optional<String> first = statementsSet.stream().filter(s -> pattern.matcher(s).matches()).findFirst();
            return first.isPresent();
        } else {
            return statementsSet.contains(sqlStatement);
        }
    }

    /**
     * Get distance like distance between sets.
     *
     * @param graph
     * @return
     */
    public Double getDistance(ClusterGraph graph) {
        Set<String> graphStatementsSet = graph.getStatementsSet();
        Sets.SetView<String> intersection = Sets.intersection(this.statementsSet, graphStatementsSet);
        int intersectSize = intersection.size();
        int graphSize = graphStatementsSet.size();
        int thisSize = this.statementsSet.size();
        Double distance = Math.max((double) (graphSize - intersectSize) / graphSize,
                (double) (thisSize - intersectSize) / thisSize);
        return distance;
    }

    @Override
    public String toString() {
        return "ClusterGraph{" +
                "statementsSet=" + statementsSet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClusterGraph that = (ClusterGraph) o;

        return entry != null ? entry.equals(that.entry) : that.entry == null;

    }

    @Override
    public int hashCode() {
        return entry != null ? entry.hashCode() : 0;
    }
}
