package amm.vsu.diploma.cluster;

import amm.vsu.diploma.domain.entity.Edge;
import amm.vsu.diploma.domain.entity.Graph;
import amm.vsu.diploma.domain.entity.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
public class ClusterGraphConverter {
    private static final String[] COLORS = new String[]{
            "#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
            "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
            "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
            "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
            "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
            "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
            "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
            "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

            "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
            "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
            "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
            "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
            "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
            "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
            "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
            "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"
    };
    private static Logger log = LoggerFactory.getLogger(ClusterGraphConverter.class);
    private int sectionSize = 80;
    private Map<Long, String> nodeIdStatement = new HashMap<>();
    private int color_index = 2;

    public String getStatementByNodeId(Long nodeId) {
        return nodeIdStatement.get(nodeId);
    }

    /**
     * Get actual graph of clusters.
     */
    public Graph getActualGraph() {
        List<Cluster> clusters = ClusteringProcessor.getInstance().getClusters();
        return convertClustersToGraph(clusters);
    }

    private Graph convertClustersToGraph(List<Cluster> clusters) {
        Graph jsGraph = new Graph();
        nodeIdStatement.clear();
        color_index = 2;
        int clustersCount = clusters.size();
        int viewSections = (int) Math.sqrt(clustersCount);
        int i = 0;
        int j = 0;
        for (Cluster cluster : clusters) {
            ClusterGraph clusterGraph = cluster.getClusterGraph();
            Graph converted = convert(cluster.getNumber(), clusterGraph,
                    i % viewSections * sectionSize,
                    j / viewSections * sectionSize);
            jsGraph.getNodes().addAll(converted.getNodes());
            jsGraph.getEdges().addAll(converted.getEdges());
            i++;
            j++;
        }
        return jsGraph;
    }

    /**
     * Get graph of clusters that contain sql.
     *
     * @param sql
     * @param isRegexp
     * @return
     */
    public Graph getGraphByStatement(String sql, boolean isRegexp) {
        List<Cluster> clustersContainsStatement =
                ClusteringProcessor.getInstance().getClustersContainsStatement(sql, isRegexp);
        return convertClustersToGraph(clustersContainsStatement);
    }

    public Graph convert(Long clusterNumber, ClusterGraph clusterGraph, int dx, int dy) {
        int nodeId = 0;
        Graph g = new Graph();
        Map<String, Long> statementNodeId = new HashMap<>();
        Random r = new Random();
        Map<String, Map<String, Integer>> entry = clusterGraph.getEntry();
        Node center = null;
        int maxNeigh = 0;
        String color = COLORS[++color_index % COLORS.length];
        for (Map.Entry<String, Map<String, Integer>> e : entry.entrySet()) {
            String k = e.getKey();
            Map<String, Integer> v = e.getValue();
            Long srcNodeId = statementNodeId.computeIfAbsent(k, l ->
                    10 * clusterNumber + (statementNodeId.size() + 1));
            nodeIdStatement.put(srcNodeId, k);
            Node srcNode = new Node(srcNodeId, "n" + srcNodeId, 3L);
            srcNode.setColor(color);
            srcNode.setX((double) (r.nextInt(sectionSize) + dx));
            srcNode.setY((double) (r.nextInt(sectionSize) + dy));
            srcNode.setInfo(k);
            if(!g.getNodes().add(srcNode))
                srcNode = g.getNodes().stream().filter(n -> n.getId() == srcNodeId).findAny().get();
            if (v.size() > maxNeigh) {
                center = srcNode;
                maxNeigh = v.size();
            }
            for (Map.Entry<String, Integer> t : v.entrySet()) {
                String vk = t.getKey();
                Long dstNodeId = statementNodeId.computeIfAbsent(vk, l ->
                        10 * clusterNumber + (statementNodeId.size() + 1));
                Node dstNode = new Node(dstNodeId, "n" + dstNodeId, 3L);
                nodeIdStatement.put(dstNodeId, vk);
                dstNode.setX((double) (r.nextInt(sectionSize) + dx));
                dstNode.setY((double) (r.nextInt(sectionSize) + dy));
                srcNode.setInfo(vk);
                dstNode.setColor(color);
                if(!g.getNodes().add(dstNode))
                    dstNode = g.getNodes().stream().filter(n -> n.getId() == dstNodeId).findAny().get();
                Edge edge = new Edge(10 * srcNodeId + dstNodeId, srcNode, dstNode);
                edge.setLabel(t.getValue() + "");
                g.getEdges().add(edge);
            }
        }
        log.debug("center {} ", center);
        return g;
    }
}
