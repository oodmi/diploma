package amm.vsu.diploma.service;

import amm.vsu.diploma.cluster.ClusterGraphConverter;
import amm.vsu.diploma.domain.entity.Graph;
import amm.vsu.diploma.repository.GraphRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GraphService {

    private final GraphRepository graphRepository;

    private final ClusterGraphConverter clusterGraphConverter;

    @Autowired
    public GraphService(GraphRepository graphRepository, ClusterGraphConverter clusterGraphConverter) {
        this.graphRepository = graphRepository;
        this.clusterGraphConverter = clusterGraphConverter;
    }

    public List<Graph> get() {
        List<Graph> graphs = new ArrayList<>();
        graphRepository.findAll().forEach(graphs::add);
        return graphs;
    }

    public Graph get(Long id) {
        return graphRepository.findOne(id);
    }

    public void create(Graph graph) {
        graphRepository.save(graph);
    }

    public void update(Long id, Graph graph) {
        graph.setId(id);
        graphRepository.save(graph);
    }

    public void delete(Long id) {
        graphRepository.delete(id);
    }

    public List<Graph> getByInformationId(Long id) {
        return graphRepository.findByInformationId(id);
    }

}
