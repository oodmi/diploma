package amm.vsu.diploma.service;

import amm.vsu.diploma.domain.entity.Edge;
import amm.vsu.diploma.repository.EdgeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EdgeService {

    private final EdgeRepository edgeRepository;

    @Autowired
    public EdgeService(EdgeRepository edgeRepository) {
        this.edgeRepository = edgeRepository;
    }

    public List<Edge> get() {
        List<Edge> edges = new ArrayList<>();
        edgeRepository.findAll().forEach(edges::add);
        return edges;
    }

    public Edge get(Long id) {
        return edgeRepository.findOne(id);
    }

    public void create(Edge edge) {
        edgeRepository.save(edge);
    }

    public void update(Long id, Edge edge) {
        edge.setId(id);
        edgeRepository.save(edge);
    }

    public void delete(Long id) {
        edgeRepository.delete(id);
    }

    public List<Edge> getByGraphId(Long id) {
        return edgeRepository.findByGraphId(id);
    }
}
