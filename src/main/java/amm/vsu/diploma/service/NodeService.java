package amm.vsu.diploma.service;

import amm.vsu.diploma.domain.entity.Node;
import amm.vsu.diploma.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NodeService {

    private final NodeRepository nodeRepository;

    @Autowired
    public NodeService(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    public List<Node> get() {
        List<Node> nodes = new ArrayList<>();
        nodeRepository.findAll().forEach(nodes::add);
        return nodes;
    }

    public Node get(Long id) {
       return nodeRepository.findOne(id);
    }

    public void create(Node node) {
        nodeRepository.save(node);
    }

    public void update(Long id, Node node) {
        node.setId(id);
        nodeRepository.save(node);
    }

    public void delete(Long id) {
        nodeRepository.delete(id);
    }

    public List<Node> getByGraphId(Long id) {
        return nodeRepository.findByGraphId(id);
    }
}
