package amm.vsu.diploma.service;

import amm.vsu.diploma.cluster.ApplicationStarter;
import amm.vsu.diploma.cluster.ClusterGraphConverter;
import amm.vsu.diploma.cluster.ClusteringProcessor;
import amm.vsu.diploma.converter.InformationConverter;
import amm.vsu.diploma.domain.entity.Edge;
import amm.vsu.diploma.domain.entity.Graph;
import amm.vsu.diploma.domain.entity.Information;
import amm.vsu.diploma.domain.entity.Node;
import amm.vsu.diploma.domain.model.data.InformationDO;
import amm.vsu.diploma.repository.EdgeRepository;
import amm.vsu.diploma.repository.GraphRepository;
import amm.vsu.diploma.repository.InformationRepository;
import amm.vsu.diploma.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class InformationService {
    private final InformationRepository informationRepository;

    private final ApplicationStarter applicationStarter;

    private final ClusterGraphConverter clusterGraphConverter;

    private final GraphRepository graphRepository;

    private final NodeRepository nodeRepository;

    private final EdgeRepository edgeRepository;

    @Autowired
    public InformationService(InformationRepository informationRepository,
                              ApplicationStarter applicationStarter,
                              ClusterGraphConverter clusterGraphConverter,
                              GraphRepository graphRepository,
                              NodeRepository nodeRepository,
                              EdgeRepository edgeRepository) {
        this.informationRepository = informationRepository;
        this.applicationStarter = applicationStarter;
        this.clusterGraphConverter = clusterGraphConverter;
        this.graphRepository = graphRepository;
        this.nodeRepository = nodeRepository;
        this.edgeRepository = edgeRepository;
    }

    public List<Information> get() {
        List<Information> information = new ArrayList<>();
        informationRepository.findAll().forEach(information::add);
        return information;
    }

    public InformationDO get(Long id) {
        if (id == -1) {
            return InformationConverter.toData(getCurrentInformation());
        } else {
            return InformationConverter.toData(informationRepository.findOne(id));
        }
    }

    public InformationDO create(Information information) {
        Information currentInformation = getCurrentInformation();
        Graph graph = currentInformation.getGraph();
        Set<Node> nodes = graph.getNodes();
        Set<Edge> edges = graph.getEdges();
        information.getGraph().getNodes().forEach(n -> {
            nodes.stream()
                    .filter(cn -> Objects.equals(cn.getId(), n.getId()))
                    .forEach(cn -> {
                        cn.setId(null);
                        cn.setX(n.getX());
                        cn.setY(n.getY());
                        cn.setGraph(graph);
                    });
        });
        edges.forEach(e -> {
            e.setGraph(graph);
            e.setId(null);
        });
        graph.setInformation(currentInformation);
        currentInformation.setStatus(ApplicationStarter.AppStatus.SAVED.getValue());
        currentInformation.setName("Сеанс " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(currentInformation.getStart()));
        informationRepository.save(currentInformation);
        nodes.forEach(nodeRepository::save);
        edges.forEach(edgeRepository::save);

        return InformationConverter.toData(informationRepository.findOne(currentInformation.getId()));
    }

    public void update(Long id, Information information) {
        information.setId(id);
        information.getGraph().getNodes().forEach(n -> {
            Node one = nodeRepository.findOne(n.getId());
            one.setX(n.getX());
            one.setY(n.getY());
            nodeRepository.save(one);
        });
    }

    public void delete(Long id) {
        informationRepository.delete(id);
    }

    private Information getCurrentInformation() {
        Information information = new Information();
        information.setGraph(clusterGraphConverter.getActualGraph());
        ClusteringProcessor clusteringProcessor = applicationStarter.getClusteringProcessor();
        information.setFulledClusters((long) clusteringProcessor.getClusters().size());
        information.setTransactionsCommit((long) clusteringProcessor.getTransactionsCommit());
        information.setTransactionsRollback((long) clusteringProcessor.getTransactionsRollback());
        information.setStatus(applicationStarter.getStatus().getValue());
        information.setStart(applicationStarter.getStart());
        information.setEnd(applicationStarter.getEnd());
        return information;
    }

    public Node getNode(Long id, Long nodeId) {
        clusterGraphConverter.getStatementByNodeId(nodeId);
        Set<Node> nodes;
        if (id == -1) {
            nodes = clusterGraphConverter.getActualGraph()
                    .getNodes();

        } else {
            nodes = informationRepository.findOne(id).getGraph().getNodes();
        }
        return nodes
                .stream()
                .filter(n -> nodeId.equals(n.getId()))
                .findFirst()
                .get();
    }

    public Graph search(Long id, String sql, Boolean isRegexp) {
        if (id == -1) {
            return clusterGraphConverter.getGraphByStatement(sql, isRegexp);
        } else {
            return null;
        }
    }
}
