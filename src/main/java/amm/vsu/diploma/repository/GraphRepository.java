package amm.vsu.diploma.repository;

import amm.vsu.diploma.domain.entity.Graph;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GraphRepository extends CrudRepository<Graph, Long> {
    List<Graph> findByInformationId(Long id);
}
