package amm.vsu.diploma.repository;

import amm.vsu.diploma.domain.entity.Information;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InformationRepository extends CrudRepository<Information, Long> {
    List<Information> findByName(String name);
}
