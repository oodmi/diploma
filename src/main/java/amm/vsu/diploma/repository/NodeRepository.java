package amm.vsu.diploma.repository;

import amm.vsu.diploma.domain.entity.Node;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NodeRepository extends CrudRepository<Node, Long> {
    List<Node> findByGraphId(Long id);
}
