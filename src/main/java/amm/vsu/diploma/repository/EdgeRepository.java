package amm.vsu.diploma.repository;

import amm.vsu.diploma.domain.entity.Edge;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EdgeRepository extends CrudRepository<Edge, Long> {
    List<Edge> findByGraphId(Long id);
}
