package amm.vsu.diploma.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Random;

public class TransactionGenerator {
    public static final Logger LOG = LoggerFactory.getLogger(TransactionGenerator.class);
    private Random random = new Random();
    private String[] statement = new String[]{
            "select ? from %s where %s",
            "insert",
            "update",
            "delete"};
    private String[] tables = new String[]{
            "Role",
            "Permission",
            "User",
            "Account",
            "Privilege",
            "Command",
            "Security_user"
    };
    private String abc = "abcdefghijkl";

    public String[] generateSimple() {
        int n = Math.abs(random.nextInt() % 4) + 3;
        String[] transact = new String[n];
        for (int i = 0; i < n; i++) {
            int j = random.nextInt(abc.length());
            transact[i] = "" + abc.charAt(j);
        }
        LOG.debug("generated {} statements {}", transact.length, Arrays.toString(transact));
        return transact;
    }

    public String[] generate() {
        int n = (random.nextInt() % 3) + 3;
        String[] transact = new String[n];
        for (int i = 0; i < n; i++) {
            int j = random.nextInt(tables.length);
            String select = statement[0];
            String statement = String.format(select, tables[j], tables[j]);
            transact[i] = statement;
        }
        return transact;
    }
}
