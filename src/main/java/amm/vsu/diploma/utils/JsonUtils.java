package amm.vsu.diploma.utils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Function;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.primitives.Longs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtils {

    public static List<Long> getParsedValues(String in) {
        if (in == null) {
            return new ArrayList<>();
        }
        return Lists.newArrayList(Iterables.transform(Splitter.on(';').split(in), new Function<String, Long>() {
            public Long apply(final String in) {
                return in == null ? null : Longs.tryParse(in.trim());
            }
        }));
    }

    public static Map<String, Object> getJsonParametersMap(String jsonParameters) throws Exception {
        try {
            JsonFactory factory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper(factory);
            mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
            TypeReference<HashMap<String, Object>> typeRef
                    = new TypeReference<HashMap<String, Object>>() {
            };
            HashMap<String, Object> parameters = mapper.readValue(jsonParameters, typeRef);
            return parameters;
        } catch (IOException e) {
            throw new Exception("Ошибка при сохранении параметра");
        }
    }

}
