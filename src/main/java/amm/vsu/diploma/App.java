package amm.vsu.diploma;

import amm.vsu.diploma.domain.entity.Edge;
import amm.vsu.diploma.domain.entity.Graph;
import amm.vsu.diploma.domain.entity.Information;
import amm.vsu.diploma.domain.entity.Node;
import amm.vsu.diploma.repository.EdgeRepository;
import amm.vsu.diploma.repository.GraphRepository;
import amm.vsu.diploma.repository.InformationRepository;
import amm.vsu.diploma.repository.NodeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

@SpringBootApplication
@Configuration
@ComponentScan("amm.vsu.diploma")
public class App extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    public CommandLineRunner demoSession(InformationRepository informationRepository,
                                         GraphRepository graphRepository,
                                         NodeRepository nodeRepository,
                                         EdgeRepository edgeRepository) {
        return (args) -> {

            Information s = new Information(new Date());
            s.setFulledClusters(3L);
            s.setTransactionsCommit(10L);
            s.setTransactionsRollback(1L);
            s.setStatus("Сохранено");
            s.setAllTime(1000L);
            s.setEnd(new Date());
            informationRepository.save(s);

            Graph graph = new Graph(s);
            graphRepository.save(graph);

            Node node = new Node(graph, 10d, 100d, "Info 11", 10L, 11d, 101d, 100d);
            nodeRepository.save(node);
            Node node1 = new Node(graph, 111d, 11d, "Info 12", 10L, 12d, 102d, 100d);
            nodeRepository.save(node1);
            Node node2 = new Node(graph, 11d, 221d, "Info 13", 10L, 13d, 103d, 100d);
            nodeRepository.save(node2);
            Node node3 = new Node(graph, 0d, 121d, "Info 14", 10L, 14d, 104d, 100d);
            nodeRepository.save(node3);
            Node node4 = new Node(graph, 60d, 80d, "Info 15", 10L, 15d, 105d, 100d);
            nodeRepository.save(node4);

            edgeRepository.save(new Edge(graph, node, node1, 4L, "e"));
            edgeRepository.save(new Edge(graph, node1, node2, 1L, "e"));
            edgeRepository.save(new Edge(graph, node2, node, 3L, "e"));
            edgeRepository.save(new Edge(graph, node, node4, 5L, "e"));
        };
    }

}
