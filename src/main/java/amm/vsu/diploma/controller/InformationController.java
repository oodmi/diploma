package amm.vsu.diploma.controller;

import amm.vsu.diploma.domain.entity.Graph;
import amm.vsu.diploma.domain.entity.Information;
import amm.vsu.diploma.domain.entity.Node;
import amm.vsu.diploma.domain.model.data.InformationDO;
import amm.vsu.diploma.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/information")
public class InformationController {

    private final InformationService informationService;

    @Autowired
    public InformationController(InformationService informationService) {
        this.informationService = informationService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Long> get() {
        return informationService.get().stream().map(Information::getId).collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public InformationDO get(@PathVariable("id") Long id) {
        return informationService.get(id);
    }

    @RequestMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public InformationDO create(@RequestBody Information information) {
        return informationService.create(information);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") Long id, @RequestBody Information information) {
        informationService.update(id, information);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        informationService.delete(id);
    }

    @RequestMapping(value = "/{id}/node/{nodeId}", method = RequestMethod.GET)
    public Node getNodeId(@PathVariable("id") Long id, @PathVariable Long nodeId) {
        return informationService.getNode(id, nodeId);
    }

    @RequestMapping(value = "/{id}/search", method = RequestMethod.GET)
    public Graph getClustersBySQL(@PathVariable("id") Long id, @RequestParam("query") String sql,
                                  @RequestParam("isregexp") Boolean isRegexp) {
        return informationService.search(id, sql, isRegexp);
    }


}
