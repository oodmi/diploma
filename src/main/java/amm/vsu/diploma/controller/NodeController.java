package amm.vsu.diploma.controller;

import amm.vsu.diploma.domain.entity.Node;
import amm.vsu.diploma.service.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/node")
public class NodeController {
    private final NodeService nodeService;

    @Autowired
    public NodeController(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Node> get() {
        return nodeService.get();
    }

    @RequestMapping(value = "/graph/{id}", method = RequestMethod.GET)
    public List<Node> getByGraphId(@PathVariable("id") Long id) {
        return nodeService.getByGraphId(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Node get(@PathVariable("id") Long id) {
        return nodeService.get(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void create(@RequestBody Node node) {
        nodeService.create(node);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") Long id, @RequestBody Node node) {
        nodeService.update(id, node);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        nodeService.delete(id);
    }
}