package amm.vsu.diploma.controller;

import amm.vsu.diploma.cluster.ApplicationStarter;
import amm.vsu.diploma.domain.model.StartParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/application")
public class MainController {
    public static final Logger logger = LoggerFactory.getLogger(MainController.class);

    private final ApplicationStarter applicationStarter;

    @Autowired
    public MainController(ApplicationStarter applicationStarter) {
        this.applicationStarter = applicationStarter;
    }

    @RequestMapping(value = "/start", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void start(@RequestBody StartParams body) {
        logger.debug("start with params = {}", body);
        applicationStarter.start(body);
    }

    @RequestMapping(value = "/stop", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void stop() {
        logger.debug("stop");
        applicationStarter.stop();
    }
}
