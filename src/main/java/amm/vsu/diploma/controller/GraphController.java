package amm.vsu.diploma.controller;

import amm.vsu.diploma.domain.entity.Graph;
import amm.vsu.diploma.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dmitry on 02.12.16.
 * Project diploma
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200",
        allowedHeaders = {"Origin", "X-Requested-With", "Content-Type", "Accept"},
        methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.OPTIONS})
@RequestMapping(value = "/graph")
public class GraphController {
    private final GraphService graphService;

    @Autowired
    public GraphController(GraphService graphService) {
        this.graphService = graphService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Graph> get() {
        return graphService.get();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Graph get(@PathVariable("id") Long id) {
        return graphService.get(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void create(@RequestBody Graph graph) {
        graphService.create(graph);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") Long id, @RequestBody Graph graph) {
        graphService.update(id, graph);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        graphService.delete(id);
    }

}
