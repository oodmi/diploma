package amm.vsu.diploma.domain.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Node implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Graph graph;

    @Column(nullable = false)
    private Double x = 0.0;

    @Column(nullable = false)
    private Double y = 0.0;

    @Column(nullable = false)
    private String label = "";

    @Column(nullable = false)
    private String info = "";

    @Column(nullable = false)
    private Long time = 0L;

    @Column(nullable = false)
    private Double dispersion = 0.0;

    @Column(nullable = false)
    private Double median = 0.0;

    @Column(nullable = false)
    private Double average = 0.0;

    @Column
    private Long size = 5L;

    @Column
    private String color = "#0f0";

    public Node() {
    }

    public Node(Long id, String label, Long size) {
        this.id = id;
        this.label = label;
        this.size = size;
    }

    public Node(Graph graph, Double x, Double y, String label, Long time, Double dispersion, Double median, Double average) {
        this.graph = graph;
        this.x = x;
        this.y = y;
        this.label = label;
        this.time = time;
        this.dispersion = dispersion;
        this.median = median;
        this.average = average;
    }

    public Node(Graph graph, Double x, Double y, String label, Long time, Double dispersion, Double median, Double average, Long size, String color) {
        this.graph = graph;
        this.x = x;
        this.y = y;
        this.label = label;
        this.time = time;
        this.dispersion = dispersion;
        this.median = median;
        this.average = average;
        this.size = size;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return id != null ? id.equals(node.id) : node.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getDispersion() {
        return dispersion;
    }

    public void setDispersion(Double dispersion) {
        this.dispersion = dispersion;
    }

    public Double getMedian() {
        return median;
    }

    public void setMedian(Double median) {
        this.median = median;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getInfo() {
        return info;
    }

    public Node setInfo(String info) {
        this.info = info;
        return this;
    }
}
