package amm.vsu.diploma.domain.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Edge implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Graph graph;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Node source;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Node target;

    @Column(nullable = false)
    private Long step = 0L;

    @Column(nullable = false)
    private String label = "";

    public Edge(){

    }

    public Edge(Long id, Node source, Node target) {
        this.source = source;
        this.target = target;
        this.id = id;
    }

    public Edge(Graph graph, Node source, Node target, Long step, String label) {
        this.graph = graph;
        this.source = source;
        this.target = target;
        this.step = step;
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        return id != null ? id.equals(edge.id) : edge.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public Long getId() {
        return id;
    }

    public Edge setId(Long id) {
        this.id = id;
        return this;
    }

    public Graph getGraph() {
        return graph;
    }

    public Edge setGraph(Graph graph) {
        this.graph = graph;
        return this;
    }

    public Node getSource() {
        return source;
    }

    public Edge setSource(Node source) {
        this.source = source;
        return this;
    }

    public Node getTarget() {
        return target;
    }

    public Edge setTarget(Node target) {
        this.target = target;
        return this;
    }

    public Long getStep() {
        return step;
    }

    public Edge setStep(Long step) {
        this.step = step;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public Edge setLabel(String label) {
        this.label = label;
        return this;
    }
}
