package amm.vsu.diploma.domain.entity;

import amm.vsu.diploma.domain.serializer.DateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Information implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name = "Новый сеанс";

    @Column(nullable = false)
    @JsonSerialize(using = DateSerializer.class)
    private Date start;

    @Column(nullable = false, name = "_end")
    @JsonSerialize(using = DateSerializer.class)
    private Date end;

    @Column
    private Long allTime = 0L;

    @Column
    private String status;

    @Column
    private Long transactionsCommit;

    @Column
    private Long transactionsRollback;

    @Column
    private Long fulledClusters;

    @OneToOne(mappedBy = "information")
    @PrimaryKeyJoinColumn
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    private Graph graph;

    public Information(){

    }

    public Information(Date date) {
        this.start = date;
        name = "Сеанс " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
    }

    public Information(String name) {
        this.name = name;
    }

    public Information(String name, Date start) {
        this.name = name;
        this.start = start;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart() {
        return this.start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return this.end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTransactionsCommit() {
        return this.transactionsCommit;
    }

    public void setTransactionsCommit(Long transactionsCommit) {
        this.transactionsCommit = transactionsCommit;
    }

    public Long getTransactionsRollback() {
        return this.transactionsRollback;
    }

    public void setTransactionsRollback(Long transactionsRollback) {
        this.transactionsRollback = transactionsRollback;
    }

    public Long getFulledClusters() {
        return this.fulledClusters;
    }

    public void setFulledClusters(Long fulledClusters) {
        this.fulledClusters = fulledClusters;
    }

    public Graph getGraph() {
        return this.graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public Long getAllTime() {
        return allTime;
    }

    public Information setAllTime(Long allTime) {
        this.allTime = allTime;
        return this;
    }
}