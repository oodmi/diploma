package amm.vsu.diploma.domain.model.data;

import java.io.Serializable;

public class EdgeDO implements Serializable {
    private Long id;
    private Long source;
    private Long target;
    private String label;

    public Long getId() {
        return id;
    }

    public EdgeDO setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getSource() {
        return source;
    }

    public EdgeDO setSource(Long source) {
        this.source = source;
        return this;
    }

    public Long getTarget() {
        return target;
    }

    public EdgeDO setTarget(Long target) {
        this.target = target;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public EdgeDO setLabel(String label) {
        this.label = label;
        return this;
    }
}