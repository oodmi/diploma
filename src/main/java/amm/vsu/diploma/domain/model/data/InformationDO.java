package amm.vsu.diploma.domain.model.data;

import amm.vsu.diploma.domain.serializer.DateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Date;

public class InformationDO implements Serializable {
    private Long id;
    private String name;
    @JsonSerialize(using = DateSerializer.class)
    private Date start;
    @JsonSerialize(using = DateSerializer.class)
    private Date end;
    private String status;
    private Long transactionsCommit;
    private Long transactionsRollback;
    private Long fulledClusters;
    private GraphDO graph;

    public Long getId() {
        return id;
    }

    public InformationDO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public InformationDO setName(String name) {
        this.name = name;
        return this;
    }

    public Date getStart() {
        return start;
    }

    public InformationDO setStart(Date start) {
        this.start = start;
        return this;
    }

    public Date getEnd() {
        return end;
    }

    public InformationDO setEnd(Date end) {
        this.end = end;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public InformationDO setStatus(String status) {
        this.status = status;
        return this;
    }

    public Long getTransactionsCommit() {
        return transactionsCommit;
    }

    public InformationDO setTransactionsCommit(Long transactionsCommit) {
        this.transactionsCommit = transactionsCommit;
        return this;
    }

    public Long getTransactionsRollback() {
        return transactionsRollback;
    }

    public InformationDO setTransactionsRollback(Long transactionsRollback) {
        this.transactionsRollback = transactionsRollback;
        return this;
    }

    public Long getFulledClusters() {
        return fulledClusters;
    }

    public InformationDO setFulledClusters(Long fulledClusters) {
        this.fulledClusters = fulledClusters;
        return this;
    }

    public GraphDO getGraph() {
        return graph;
    }

    public InformationDO setGraph(GraphDO graph) {
        this.graph = graph;
        return this;
    }
}