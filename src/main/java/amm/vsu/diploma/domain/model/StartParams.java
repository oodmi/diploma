package amm.vsu.diploma.domain.model;

public class StartParams {
    private int maxClusters;
    private double minThreshold;

    public int getMaxClusters() {
        return maxClusters;
    }

    public StartParams setMaxClusters(int maxClusters) {
        this.maxClusters = maxClusters;
        return this;
    }

    public double getMinThreshold() {
        return minThreshold;
    }

    public StartParams setMinThreshold(double minThreshold) {
        this.minThreshold = minThreshold;
        return this;
    }

    @Override
    public String toString() {
        return "StartParams{" +
                "maxClusters=" + maxClusters +
                ", minThreshold=" + minThreshold +
                '}';
    }
}

