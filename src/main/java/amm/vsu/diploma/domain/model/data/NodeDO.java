package amm.vsu.diploma.domain.model.data;

import java.io.Serializable;

public class NodeDO implements Serializable {
    private Long id;
    private Double x;
    private Double y;
    private String label;
    private Long size;
    private String color;

    public Long getId() {
        return id;
    }

    public NodeDO setId(Long id) {
        this.id = id;
        return this;
    }

    public Double getX() {
        return x;
    }

    public NodeDO setX(Double x) {
        this.x = x;
        return this;
    }

    public Double getY() {
        return y;
    }

    public NodeDO setY(Double y) {
        this.y = y;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public NodeDO setLabel(String label) {
        this.label = label;
        return this;
    }

    public Long getSize() {
        return size;
    }

    public NodeDO setSize(Long size) {
        this.size = size;
        return this;
    }

    public String getColor() {
        return color;
    }

    public NodeDO setColor(String color) {
        this.color = color;
        return this;
    }
}
