package amm.vsu.diploma.domain.model.info;

import java.util.Date;
import java.util.List;

public class ExecutionInfo {
    private String name;
    private Date date = new Date();
    private Long time;
    private Long tid;
    private boolean success;
    private String type;
    private boolean batch;
    private Long querySize;
    private Long batchSize;
    private List<String> query;
    private List<List<String>> params;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public ExecutionInfo setDate(Date date) {
        this.date = date;
        return this;
    }

    public Long getTime() {
        return time;
    }

    public ExecutionInfo setTime(Long time) {
        this.time = time;
        return this;
    }

    public Long getTid() {
        return tid;
    }

    public ExecutionInfo setTid(Long tid) {
        this.tid = tid;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public ExecutionInfo setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public String getType() {
        return type;
    }

    public ExecutionInfo setType(String type) {
        this.type = type;
        return this;
    }

    public boolean isBatch() {
        return batch;
    }

    public void setBatch(boolean batch) {
        this.batch = batch;
    }

    public Long getQuerySize() {
        return querySize;
    }

    public void setQuerySize(Long querySize) {
        this.querySize = querySize;
    }

    public Long getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(Long batchSize) {
        this.batchSize = batchSize;
    }

    public List<String> getQuery() {
        return query;
    }

    public void setQuery(List<String> query) {
        this.query = query;
    }

    public List<List<String>> getParams() {
        return params;
    }

    public void setParams(List<List<String>> params) {
        this.params = params;
    }
}