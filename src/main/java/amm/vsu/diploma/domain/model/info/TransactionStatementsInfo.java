package amm.vsu.diploma.domain.model.info;


import amm.vsu.diploma.enums.Status;

import java.time.Duration;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

public class TransactionStatementsInfo {
    private SortedSet<ExecutionInfo> statements;
    private Long tid;
    private Status status;
    private Date dateEnd;
    private int countStatusStatements = 0;

    public TransactionStatementsInfo(Long tid) {
        statements = new TreeSet<>(
                (o1, o2) -> o1.getDate().compareTo(o2.getDate()));
        this.tid = tid;
        status = Status.UNKNOW;
    }

    public boolean isFull() {
        return countStatusStatements == statements.size();
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getCountStatusStatements() {
        return countStatusStatements;
    }

    public void setCountStatusStatements(int countStatusStatements) {
        this.countStatusStatements = countStatusStatements;
    }

    public void addStatementInfo(ExecutionInfo statementInfo) {
        getStatements().add(statementInfo);
    }

    public SortedSet<ExecutionInfo> getStatements() {
        return statements;
    }

    public void setStatements(SortedSet<ExecutionInfo> statements) {
        this.statements = statements;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getStartTime() {
        return statements.first().getDate();
    }

    public Date getEndTime() {
        return dateEnd;
    }

    public Duration getExecutionTime() {
        return Duration.between(getStartTime().toInstant(), getEndTime().toInstant());
    }
}
