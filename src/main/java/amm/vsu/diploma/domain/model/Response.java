package amm.vsu.diploma.domain.model;

import amm.vsu.diploma.enums.ResponseStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    private static final Logger log = LoggerFactory.getLogger(Response.class);

    private ResponseStatus status;
    private Object data;
    private String error;

    private Response(ResponseStatus status, Object data, String error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static Response from(Supplier<Object> from) {
        try {
            return Response.success(from.get());
        } catch (Throwable t) {
            log.error("Error occurred while building response.", t);
            return Response.fail(t.getMessage());
        }
    }

    public static Response success() {
        return new Response(ResponseStatus.OK, null, null);
    }

    public static Response success(Object data) {
        return new Response(ResponseStatus.OK, data, null);
    }

    public static Response fail(String error) {
        return new Response(ResponseStatus.ERROR, null, error);
    }

    public static Response fail(String error, Object data) {
        return new Response(ResponseStatus.ERROR, data, error);
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
