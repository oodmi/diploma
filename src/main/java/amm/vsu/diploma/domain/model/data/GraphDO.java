package amm.vsu.diploma.domain.model.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class GraphDO implements Serializable {
    private Long id;
    private Set<NodeDO> nodes;
    private Set<EdgeDO> edges;

    public GraphDO() {
        nodes = new HashSet<>();
        edges = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public GraphDO setId(Long id) {
        this.id = id;
        return this;
    }

    public Set<NodeDO> getNodes() {
        return nodes;
    }

    public GraphDO setNodes(Set<NodeDO> nodes) {
        this.nodes = nodes;
        return this;
    }

    public Set<EdgeDO> getEdges() {
        return edges;
    }

    public GraphDO setEdges(Set<EdgeDO> edges) {
        this.edges = edges;
        return this;
    }
}
