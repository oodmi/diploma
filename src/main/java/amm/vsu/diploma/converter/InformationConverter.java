package amm.vsu.diploma.converter;


import amm.vsu.diploma.domain.entity.Information;
import amm.vsu.diploma.domain.model.data.EdgeDO;
import amm.vsu.diploma.domain.model.data.GraphDO;
import amm.vsu.diploma.domain.model.data.InformationDO;
import amm.vsu.diploma.domain.model.data.NodeDO;

public class InformationConverter {
    public static InformationDO toData(Information information) {
        InformationDO info = new InformationDO();
        info.setId(information.getId())
                .setFulledClusters(information.getFulledClusters())
                .setTransactionsCommit(information.getTransactionsCommit())
                .setTransactionsRollback(information.getTransactionsRollback())
                .setEnd(information.getEnd())
                .setStart(information.getStart())
                .setName(information.getName())
                .setStatus(information.getStatus());
        GraphDO graph = new GraphDO();
        graph.setId(information.getGraph().getId());
        information.getGraph().getNodes().forEach(n -> {
            graph.getNodes().add(new NodeDO()
                    .setId(n.getId())
                    .setColor(n.getColor())
                    .setLabel(n.getLabel())
                    .setSize(n.getSize())
                    .setX(n.getX())
                    .setY(n.getY())
            );
        });
        information.getGraph().getEdges().forEach(e -> {
            graph.getEdges().add(new EdgeDO()
                    .setId(e.getId())
                    .setLabel(e.getLabel())
                    .setSource(e.getSource().getId())
                    .setTarget(e.getTarget().getId())
            );
        });
        info.setGraph(graph);
        return info;
    }
}
