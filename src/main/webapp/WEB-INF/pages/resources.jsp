<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<link type="text/css" rel="stylesheet" href="<c:url value="/static/materialize/css/materialize.min.css"/>"
      media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="<c:url value="/static/css/style.css"/>"/>
<link href="<c:url value="/static/css/style.css"/>" rel="stylesheet"/>

<script type="text/javascript" src="<c:url value="/static/jquery/jquery-1.12.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/materialize/js/materialize.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/sigmajs/sigma.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/sigmajs/sigma.parsers.json.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/sigmajs/plugins/sigma.layout.forceAtlas2.min.js"/>"></script>
<script type="text/javascript"
        src="<c:url value="/static/sigmajs/plugins/sigma.renderers.edgeLabels.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/sigmajs/plugins/sigma.plugins.dragNodes.min.js"/>"></script>
<script type="text/javascript" src="<c:url value='/static/mustache/mustache.min.js'/>"></script>

<script id="session-information-template" type="text/x-handlebars-template">
    <div class="card-panel teal" id="session">
        <table class="white-text">
            <caption><h5>Информация о сеансе</h5></caption>
            <tbody>
            <tr>
                <td>Название</td>
                <td>{{name}}</td>
            </tr>
            <tr>
                <td>Статус работы</td>
                <td id="m_status">{{status}}</td>
            </tr>
            <tr>
                <td>Начало работы</td>
                <td>{{start}}</td>
            </tr>
            <tr>
                <td>Конец работы</td>
                <td>{{end}}</td>
            </tr>
            <tr>
                <td>Заполнено кластеров</td>
                <td>{{fulledClusters}}</td>
            </tr>
            <tr>
                <td>Зафиксированно транзакций</td>
                <td>{{transactionsCommit}}</td>
            </tr>
            <tr>
                <td>Откатанно транзакций</td>
                <td>{{transactionsRollback}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</script>
<script id="node-information-template" type="text/x-handlebars-template">
    <div class="card-panel teal" id="node">
        <div class="white-text">
            <table class="white-text">
                <caption><h5>Информация об узле</h5></caption>
                <tbody>
                <tr>
                    <td>Узел</td>
                    <td>{{label}}</td>
                </tr>
                <tr>
                    <td>Среднее время</td>
                    <td>{{average}}</td>
                </tr>
                <tr>
                    <td>Дисперсия</td>
                    <td>{{dispersion}}</td>
                </tr>
                <tr>
                    <td>Медиана</td>
                    <td>{{median}}</td>
                </tr>
                <tr>
                    <td>Запрос</td>
                    <td>{{info}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script type="text/javascript">
    var url = "${pageContext.request.contextPath}";
    var currentSession = -1;

    $(document).ready(function () {
        if (window.sigmaInst) {
            getInformation();
        }
        (function () {
            document.onclick = function (event) {
                var target = event.target;

                var id = target.getAttribute('data-toggle-id');
                if (!id) return;

                var elem = document.getElementById(id);

                elem.hidden = !elem.hidden;
            };
        })();
        $('.dropdown-button').dropdown({
                    belowOrigin: true
                }
        );
        showButton(true, false, false, false, false);
    });

    function start() {
        currentSession = -1;
        var mc = $("#num_clusters").val();
        var mt = $("#thr").val();
        var startData = {
            maxClusters: mc,
            minThreshold: mt
        };
        $.ajax({
            url: url + "/application/start/",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify(startData),
            success: function (data, status) {
                $("#m_status").text("Выполняется");
                Materialize.toast('Сбор начат!', 1000)
                showButton(false, true, true, false, false);
            },
            error: function (xhr, status, error) {
                console.log("error " + xhr + status + error)
            }
        });
    }

    function stop() {
        $.ajax({
            url: url + "/application/stop/",
            type: "post",
            contentType: "application/json",
            success: function (data, status) {
                console.log("status: " + status);
                $("#m_status").text("Остановлено");
                Materialize.toast('Статистика собрана!', 1000);
                showButton(true, false, true, true, false);
            },
            error: function (xhr, status, error) {
                console.log("error " + xhr + status + error)
            }
        });
    }

    function save() {
        if (currentSession == -1) {
            $.ajax({
                url: url + "/information/",
                type: "POST",
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(getCurrentInformation()),
                success: function (data, status) {
                    currentSession = data.id;
                    showGraph(data.graph);
                    showMonitoringInfo(data);
                    addInfoToHtml(data);
                    showButton(false,false,false,true,true)
                },
                error: function (xhr, status, error) {
                    console.log("error " + xhr + status + error)
                }
            });
        } else {
            $.ajax({
                url: url + "/information/" + currentSession,
                type: "PUT",
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(getCurrentInformation()),
                success: function (data, status) {
                    showButton(false,false,false,true,true)
                },
                error: function (xhr, status, error) {
                    console.log("error " + xhr + status + error)
                }
            });
        }
    }

    function deleteInfo() {
        $.ajax({
            url: url + "/information/" + currentSession,
            type: "DELETE",
            success: function (status) {
                console.log("status: " + status);
                deleteInfoFromHtml();
            },
            error: function (xhr, status, error) {
                console.log("error " + xhr + status + error)
            }
        });
    }

    function getInformation() {
        $.get(url + "/information/" + currentSession, function (data, status) {
            showGraph(data.graph);
            showMonitoringInfo(data);
            Materialize.toast('Построение графа!', 1000)
        });
    }

    function getStatementInfo(node) {
        $.get(url + "/information/" + currentSession + "/node/" + node.id, function (data, status) {
            console.log("received statement: " + data);
            showSqlStatementInfo(node.label, data);
        });
    }
    function searchInfo() {
        var sql_query = $("#sql_query").val();
        var is_regexp = $("#is_regexp").prop("checked");
        $.get(url + "/information/" + "-1" + "/search?query=" + sql_query + "&isregexp=" + is_regexp, function (data, status) {
            console.log("status: " + status);
            showGraph(data);
        });
    }

    function waitForRender() {
        console.log("wait for render start");
        setTimeout('window.sigmaInst.stopForceAtlas2(); console.log("wait for render is done")', 3000);
    }

    function refresh() {
        if (!window.sigmaInst) {
            window.sigmaInst = new sigma({
                renderer: {
                    container: document.getElementById('canva'),
                    type: 'canvas' // sigma.renderers.canvas works as well
                },
                settings: {
                    defaultNodeColor: '#ec5148',
                    defaultEdgeColor: '#ccc',
                    defaultEdgeType: 'curvedArrow',
                    doubleClickEnabled: false,
                    autoResize: false,
                    autoRescale: false
                }
            });
            // Initialize the dragNodes plugin:
            var dragListener = sigma.plugins.dragNodes(sigmaInst, sigmaInst.renderers[0]);
            sigmaInst.bind('doubleClickNode', function (e) {
                console.log(e.type, e.data.node.label, e.data.captor);
                getStatementInfo(e.data.node);
            });
        }
        getInformation();
    }
    function showGraph(data) {
        window.sigmaInst.graph.clear();
        window.sigmaInst.graph.read(data);
        window.sigmaInst.refresh();
//        window.sigmaInst.startForceAtlas2();
        waitForRender();
    }
    function getCurrentInformation() {
        var nodes = window.sigmaInst.graph.nodes();
        var edges = window.sigmaInst.graph.edges();
        var g = {
            nodes: nodes,
            edges: edges
        };
        var information = {};
        information.graph = g;
        return information;
    }
    function changeInformation(id) {
        $('#nodeInfo').html("");
        currentSession = id;
        refresh();
        showButton(false,false,false,true,true)
    }
    function addInfoToHtml(data) {
        var l = document.createElement('li');
        l.id = "info_" + data.id;
        l.innerHTML = "<a onclick='changeInformation(" + data.id + ")'>" + data.name + "</a>";
        document.getElementById("dropdown-list").appendChild(l);
    }
    function deleteInfoFromHtml() {
        $("#info_" + currentSession).remove();
        clear();
    }
    function showSqlStatementInfo(nodeName, data) {
        var modalTemplate = $("#node-information-template").html();
        var html = Mustache.to_html(modalTemplate, data);
        $('#nodeInfo').html(html);
    }
    function showMonitoringInfo(data) {
        var modalTemplate = $("#session-information-template").html();
        var html = Mustache.to_html(modalTemplate, data);
        $('#sessionInfo').html(html);
    }
    function disable(id, show) {
        if (!show)
            $("#" + id).removeClass("disabled");
        else
            $("#" + id).addClass("disabled")
    }
    function showButton(s1, s2, s3, s4, s5) {
        disable("btn_start", !s1);
        disable("btn_stop", !s2);
        disable("btn_refresh", !s3);
        disable("btn_save", !s4);
        disable("btn_delete", !s5);
    }
    function newSession() {
        currentSession = -1;
        clear();
    }
    function clear() {
        window.sigmaInst.graph.clear();
        window.sigmaInst.refresh();
        $('#nodeInfo').html("");
        $('#sessionInfo').html("");
        showButton(true, false, false, false, false);
    }
</script>