<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <jsp:include page="resources.jsp"/>
</head>
<body>
<header>
    <div class="navbar-fixed">
        <nav class="top-nav">
            <div class="container">
                <div class="nav-wrapper">
                    <a class="page-title">Мониторинг транзакций</a>
                    <ul class="right hide-on-med-and-down">
                        <li>
                            <a class="waves-effect waves-light btn" id="bnt_new" onclick="newSession()">
                                Новый сеанс
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-button btn" data-activates="dropdown-list">
                                Сохраненные сеансы
                                <i class="material-icons right">arrow_drop_down</i>
                            </a>
                        </li>
                        <ul id='dropdown-list' class='dropdown-content'>
                            <c:forEach items="${infoList}" var="info">
                                <li id="info_${info.id}">
                                    <a onclick="changeInformation(${info.id})">${info.name}</a>
                                </li>
                                <li class="divider"></li>
                            </c:forEach>
                        </ul>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col s8">
                <form>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="num_clusters" type="text" class="validate" data-error="Ошибка ввода"
                                   pattern="[0-9]*"
                                   required>
                            <label for="num_clusters">Максимальное число кластеров</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="thr" type="text" class="validate" pattern="[0]\.[0-9]{1,2}"
                                   data-error="Число от 0 до 1">
                            <label for="thr">Порог метрики</label>
                        </div>
                        <div class="col s12" style="margin-bottom: 20px">
                            <label data-toggle-id="sqlSearch" id="extraFeature">Дополнительно</label>
                            <div id="sqlSearch" hidden>
                                <div class="input-field col s6">
                                    <input id="sql_query" type="text" class="validate">
                                    <label for="sql_query">SQL оператор</label>
                                </div>
                                <div class="input-field col s3">
                                    <div class="checkbox">
                                        <input id="is_regexp" type="checkbox">
                                        <label for="is_regexp">Регулярное выражение</label>
                                    </div>
                                </div>
                                <div class="input-field col s3">
                                    <div>
                                        <a id="btn_search" class="waves-effect waves-light btn"
                                           onclick="searchInfo()">Найти</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12" style="display: inline-block">
                            <a id="btn_start" class="waves-effect waves-light btn" onclick="start()">Старт</a>
                            <a id="btn_stop" class="waves-effect waves-light btn" onclick="stop()">Стоп</a>
                            <a id="btn_refresh" class="waves-effect waves-light btn" onclick="refresh()">
                                Обновить</a>
                            <a id="btn_save" class="waves-effect waves-light btn" onclick="save()">Cохранить</a>
                            <a id="btn_delete" class="waves-effect waves-light btn"
                               onclick="deleteInfo()">Удалить</a>
                        </div>
                    </div>
                </form>
                <div id="canva" style="border: 1px #009688 solid">
                </div>
            </div>
            <div class="col s4">
                <div id="sessionInfo">
                </div>
                <div id="nodeInfo">
                </div>
            </div>
        </div>
    </div>
</main>
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2017 Dmitry Ovchinnikov
            <a class="grey-text text-lighten-4 right" href="http://github.com/oodmi">Git</a>
        </div>
    </div>
</footer>
</body>
</html>